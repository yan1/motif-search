
# Workflow for 11 spacers (1280 well sequence data )##

## 1. I/O

### 1. 1 Input file ###

1. 590137 reads sequenced by ONT’s Guppy. 
Location: bonette:/media/ssd/ngs-data-analysis/2024_motif/EIC01-01-1280_reads.fastq

2. The payload motif library, 202401-1280well-sequence/input_file/motif-lib.fa (index from m1)

3. The spacer motif library, 202401-1280well-sequence/input_file/spacers.fa (index from s1)

4. The 24nt ONT barcodes (96 barcodes in total), 202401-1280well-sequence/input_file/barcode.fa
NOTE: don't include the patten in barcode.fa!!!!!
```
>NB_1st
AGGTTAANNNNNNNNNNNNNNNNNNNNNNNNCAGCACCT
>NB_2nd
ATTGCTAAGGTTAANNNNNNNNNNNNNNNNNNNNNNNNCAGCACC
```
4. Forward primer `ACTTACCCGCTACTTTCATCACTAATTTACTCCCAAC`, reverse primer `CCAACTACACTATACCACCTCGCATCATACCTACCTTA`

5. The oligo structure `5flank--Addr0--Addr1--Payload1--Payload2--Payload3--Payload4--Payload5--Payload6--Payload7--Payload8-3flank`


### 1. 2 Output format ###
columns separated by '\t' in format of `rname direction pos as motifs`
rname: read name
direction: fwd(+)/rev(-)
pos: the start position of the flanked motif. 
as: alignment score of each motif
motifs: the name of decoded motifs

Note: a fake motif means that it is not able to find any matching motif, the pos and as are set to 0.

Example:
@@@0117ec74	+	Pos:75-75-125-172-222-273-464-515-566-616-	AS:0-24-100-48-42-70-40-70-90-70-	Motifs:>NB01->fake->m1->m1->m3->m1->m4->m5->m7->m1->m4


#### Decode

##### inference

```
code=/home/yiqing/code/motif-search
path=/home/yiqing/motif-search
threads=12

time $code/motif-search -t $threads \
-r  $path/EIC01-01-1280_reads.fastq -i guppy \
-m $path/motif-lib.fa --n 10 \
--fp ACTTACCCGCTACTTTCATCACTAATTTACTCCCAAC \
--rp CCAACTACACTATACCACCTCGCATCATACCTACCTTA \
--s $path/spacers.fa --mode L \
--b $path/barcode.fa \
-o $path/res/res-loose-chain.txt 1>>$path/res/pipe.log 2>&1
```


12 threads run 28 mins on bonette4, 590137 lines output

48t 290min

```
gdb motif-search 
set history save on

set args -t 1 -r  /home/yiqing/motif-search/r0117.fq  -i guppy -m /home/yiqing/motif-search/motif-lib.fa --n 10 --fp ACTTACCCGCTACTTTCATCACTAATTTACTCCCAAC --rp CCAACTACACTATACCACCTCGCATCATACCTACCTTA --s /home/yiqing/motif-search/spacers.fa --mode L --b /home/yiqing/motif-search/barcode.fa 
```

##### consensus
```
## get top N frequent motif in each column, generate helix format, check recover rate
python /media/ssd/ngs-data-analysis/code/motif-search/script/202401-1280well-sequence/consensus.py \

```




