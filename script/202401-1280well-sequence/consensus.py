from __future__ import division

import pandas as pd
import sys
import csv

nb_barcode = 20
nb_seq = 64
N = 4

workspace = '/Users/yan/Desktop/eurecom_code/motif-search-gitlab'
input_file = workspace + '/res/res-barcode-short-new-barcode.txt'
output_path = workspace + '/res/'

#########  load inferred motifs #########
colName = ["res"]
colIndex = [4]
motifs = pd.read_csv(input_file, sep="\t", header=None, usecols=colIndex, names=colName, quoting=csv.QUOTE_NONE)
m_list = motifs['res'].str.split('->',expand=True)
m_list[0] = m_list[0].str[1:]
m_list_NB = m_list[m_list[0].str.contains("NB")]
print('nb of res:' + str(len(m_list)))
print('nb of res has barcode:' + str(len(m_list_NB)))

######### Generate barcode name #########
######### from 'NB_exp_id' to 'NB80' with step 4 #########
exp_id = 4

end_number = 80
step = 4
start_prefix = 'NB'
barcode_list = [f"{start_prefix}{i:02d}" for i in range(exp_id, end_number + 1, step)]
print(barcode_list)
selected_barcode = m_list_NB[m_list_NB[0].isin(barcode_list)]
selected_barcode.head(2)

#########  get the topN frequent motif per barcode/addres0/addres1 #########
for i in range(8):
    j = i + 3
    non_fake = selected_barcode[(selected_barcode[1]!='fake') & (selected_barcode[2]!='fake') & (selected_barcode[j]!='fake')]
    topN_frequent = non_fake.groupby([0, 1, 2])[j].value_counts().groupby(level=[0, 1, 2]).head(N).reset_index(name='Count')
    # good_addr= topN_frequent[topN_frequent['Count'] > 0.75 * topN_frequent['Count'].mean()]
    good_addr = topN_frequent.sort_values('Count', ascending=False).head(nb_barcode* nb_seq * N).sort_values([0, 1, 2])
    good_addr.to_csv(output_path + 'T' + str(exp_id) + 'good_addr' + str(j-2) + '.csv', index = None)


#########  load the reference/encoding file #########
ref_dir = '/Users/yan/Desktop/eurecom_code/motif-search-gitlab/script/202401-1280well-sequence/input_file/'
ref_path = ref_dir + 'EIC01-01-1280-T' + str(exp_id) + '_encoded_clean.tsv'
ref = pd.read_csv(ref_path, sep="\t", header = 0, quoting=csv.QUOTE_NONE)
print('nb of reference: ' + str(len(ref)))
ref.head()

res = []

for i in range(8):
    # i = 02
    col_name = str(i + 3)
    payload_name = "Payload" + str(i+1)
    input_path =  output_path + 'T' + str(exp_id) + "good_addr" + str(i+1) + ".csv"
    ######### load the decoded motifs of column "col_name" #########
    motifs = pd.read_csv(input_path, sep=",", header = 0, quoting=csv.QUOTE_NONE)
    motifs[col_name] = motifs[col_name].str[1:].astype(int)
    motifs = motifs.groupby(['0', '1', '2'])[col_name].apply(list).reset_index()
    motifs[col_name] = motifs[col_name].apply(sorted)
    motifs = motifs.sort_values(['0', '2'])
    motifs['ONT_Barcode'] = motifs['0'].str[2:].astype(int)
    motifs['HW_Address'] = 'barcode_external0' + motifs['1'].str[1:] + '_internal0' + motifs['2'].str[1:]
    motifs[['ONT_Barcode', 'HW_Address', col_name]].to_csv(output_path + 'recover-T' + str(exp_id) + '-Payload' + str(i+1) + '.csv', index = False)
    ######### get the recover accuracy #########
    merged = pd.merge(motifs, ref, how='outer', left_on=['ONT_Barcode', 'HW_Address'], right_on=['ONT_Barcode', 'HW_Address'])
    cmp_res = merged[col_name].astype(str) == merged[payload_name].astype(str)
    import ast
    merged[payload_name] = merged[payload_name].apply(ast.literal_eval) # convert string to list
    merged.loc[merged[col_name].isnull(),[col_name]] = merged.loc[merged[col_name].isnull(),col_name].apply(lambda x: []) # replace NaN with []
    merged['nb_same_motif'] = merged.apply(lambda row: len(set(row[col_name]) & set(row[payload_name])), axis=1)
    nb_same_motif = merged['nb_same_motif'].sum()
    res.append([cmp_res.sum(), nb_same_motif])
    ######### generate the decode result in Helix format #########
    if i == 0:
        full_decode = motifs[['ONT_Barcode', 'HW_Address', col_name, '1', '2']]
    else:
        full_decode = pd.merge(full_decode, motifs[['ONT_Barcode', 'HW_Address', col_name, '1', '2']], how='outer', left_on=['ONT_Barcode', 'HW_Address', '1', '2'], right_on=['ONT_Barcode', 'HW_Address', '1', '2'])
    full_decode.rename(columns={col_name: 'Payload'+str(i+1)}, inplace=True)

######### output the decode result in Helix format #########
full_decode = full_decode.sort_values(['ONT_Barcode', '2', '1'])
full_decode.drop(columns=['1', '2']).to_csv(output_path + 'ms-decoded-T' + str(exp_id) + '.csv', sep='\t', index = False)

######### output the recover accuracy #########
res_df = pd.DataFrame(res)
res_df.rename(columns={0: 'fully recovered seq', 1: 'fully recovered motif'}, inplace=True)
res_df.to_csv(output_path + 'recover-T' + str(exp_id) + '.csv', index = True)
seq_percent = res_df['fully recovered seq'].sum()/(20*8*8*8)
motif_percent = res_df['fully recovered motif'].sum()/(8*8*20*4*8)
print("Nb of sequence fully recovered: " + str(res_df['fully recovered seq'].sum()) + ', ' + str(seq_percent))
print("Nb of motif fully recovered: " + str(res_df['fully recovered motif'].sum()) + ', ' + str(motif_percent))