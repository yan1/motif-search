from __future__ import division

import pandas as pd
import sys
import csv

input_file = str(sys.argv[1])
output_file = input_file[:-4]
# input_file = "res-cov1.txt"
# output_file = "res-cov1"

nb_seq = 22
N = 4

folder = '/media/ssd/ngs-data-analysis/motif-search-8payload-202305/res/'
input_path = folder + input_file
print("get the top N frequent motifs from " + input_path)

output_path = folder + output_file + '-'

colName = ["res"]
colIndex = [4]

motifs = pd.read_csv(input_path, sep="\t", header=None, usecols=colIndex, names=colName, quoting=csv.QUOTE_NONE)
m_list = motifs['res'].str.split('->',expand=True)
m_list[0] = m_list[0].str[1:]

for i in range(8):
    j = i + 2
    topN_frequent = m_list.groupby([0, 1])[j].value_counts().groupby(level=[0, 1]).head(N).reset_index(name='Count')
    # good_addr= topN_frequent[topN_frequent['Count'] > 0.75 * topN_frequent['Count'].mean()]
    good_addr = topN_frequent.sort_values('Count', ascending=False).head(nb_seq * N).sort_values([0, 1])
    good_addr.to_csv(output_path + 'good_addr' + str(j) + '.csv', index = None)
