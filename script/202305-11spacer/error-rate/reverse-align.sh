# bonette server
# ./reverse-align.sh 1>>/media/ssd/ngs-data-analysis/motif-search-8payload-202305/res/reverse-align.log 2>&1

klen=14
threads=12
in_path=/media/ssd/ngs-data-analysis/motif-search-8payload-202305
#filename=/guppy/guppy-mapped

## clean up the result (because will use >>)
mkdir -p $in_path/mini-res
rm -f $in_path/mini-res/*


nline=$(wc -l < $in_path/read/all_pass.txt)
index_elapsed=0
align_elapsed=0

for i in $(seq 1 $nline); do
  i=70
  echo "processing ${i}th line"
  out_path=$in_path/$i/
  mkdir $out_path

  # build ref fasta file based on the i'th read
  echo '>'${i}'' >$out_path/r-ref
  awk '{if(NR=='${i}') print }' $in_path/read/all_pass.txt >>$out_path/r-ref

  #index
  start_time="$(date -u +%s.%N)"
  time /media/ssd/ngs-data-analysis/code/minimap2/minimap2 -t $threads -k $klen -w $klen \
    -d $out_path/mini-$klen.mmi $out_path/r-ref
  end_time="$(date -u +%s.%N)"
  elapsed="$(bc <<<"$end_time-$start_time")"
  index_elapsed=$(echo $elapsed+$index_elapsed | bc)

  #align the oligos to the ref(read)
  start_time="$(date -u +%s.%N)"
  time /media/ssd/ngs-data-analysis/code/minimap2/minimap2 \
    -t $threads -ax map-ont $out_path/mini-$klen.mmi $in_path/ref/ref.fq \
    >$out_path/minimap2.sam
  end_time="$(date -u +%s.%N)"
  elapsed="$(bc <<<"$end_time-$start_time")"
  align_elapsed=$(echo $elapsed+$align_elapsed | bc)

  # extract the aligned result
  awk '$3!="*" {print}' $out_path/minimap2.sam >>$out_path/minimap2-aligned.sam
  python /media/ssd/ngs-data-analysis/code/motif-search/script/202305-11spacer/error-rate/get_highest_AS_align.py $i

  rm -r $out_path
done

echo "yyan-log m1:${m1}, m2:${m2}, cov:$cov index time: $index_elapsed"
echo "yyan-log m1:${m1}, m2:${m2}, cov:$cov align time: $align_elapsed"

#python /media/ssd/ngs-data-analysis/code/motif-search/script/final/fscore-mn.py $m1 $m2 $cov

