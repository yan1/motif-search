import numpy as np
import pandas as pd
import sys
import csv

id = int(sys.argv[1])
# id=70
out_path = '/media/ssd/ngs-data-analysis/motif-search-8payload-202305/mini-res/minimap2.sam'

colName = ["rname", "AS"]
colIndex = [0, 13]

sam_path = '/media/ssd/ngs-data-analysis/motif-search-8payload-202305/' + str(id) + '/minimap2-aligned.sam'
sam = pd.read_csv(sam_path, sep = "\t", header = None, skiprows = 3, usecols= colIndex, names = colName, quoting=csv.QUOTE_NONE)
sam = sam[sam['AS'].notna()]

if len(sam) == 0:
    line = str(id) + "th line in the read\t*\t\n"
    with open(out_path, 'a') as fileB:
        fileB.write(line)
else:
    sam["AS"] =  sam["AS"].str.split(':').str[2].astype('int')
    max_index = sam["AS"] .idxmax()
    row_with_max_value = sam['rname'].loc[max_index]
    with open(sam_path, 'r') as fileA, open(out_path, 'a') as fileB:
        for line in fileA:
            if line.startswith(str(row_with_max_value)):
                fileB.write(line)