
# Workflow for 11 spacers ##

## 1. Preparation

### 1. 1 Input file ###

Read: 1133 pass read fastq files of 3,224,497 reads. 
Merge the read fastq files into one file as `all_pass.fq`.

### 1. 2 Read length histogram ###
`script/202305-11spacer/stats.py`

### 1. 3 Error rate per base ###

#### 1. 3. 1 Align read to reference --> NOT WORK ###

```shell
path=/media/ssd/ngs-data-analysis/motif-search-8payload-202305

### build all possible oligos in ref.fa file
python script/202305-11spacer/build-ref.py

### index with minimap2
/media/ssd/ngs-data-analysis/code/minimap2/minimap2 -t 12 \
  -d $path/ref/mini-1000.mmi $path/ref/ref-1000.fa
  
### align the reads to the ref with minimap2 
time /media/ssd/ngs-data-analysis/code/minimap2/minimap2 \
-t 12 -ax sr $path/ref/mini-8.mmi $path/read/all_pass.fq \
> $path/res/minimap2.sam 

time /media/ssd/ngs-data-analysis/code/minimap2/minimap2 \
-t 12 -ax map-ont $path/ref/mini-1000.mmi $path/read/all_pass-cov100.fq \
> $path/res/minimap2.sam 


time /media/ssd/ngs-data-analysis/code/minimap2/minimap2 \
-t 12 -ax map-ont $path/ref/mini.mmi $path/read/all_pass_len_300_600.fq \
> $path/res/minimap2.sam 

```


#### 1. 3. 2 Reversed alignment: align reference to read ###
```
## read: convert fq to txt format (only get the seq)
python accalign-benchmark/common/format/fq2string.py
## reference oligos: convert fa to fq format
python accalign-benchmark/common/format/fa2fq.py 

sh error-rate/reverse-align.sh
```

#### 1.3.3 Get error rate per position with bbmap

```
# Note: bbmap only works with length 600,so filter out read longer than 600 by /pre/filterByReadLen.py
python accalign-benchmark/common/format/filterFqReadLen.py`

path=/media/ssd/ngs-data-analysis/motif-search-8payload-202305
/media/ssd/ngs-data-analysis/code/bbmap_38.81/bbmap.sh \
ref=$path/ref/ref.fa \
in=$path/read/all_pass_len_300_600.fq \
out=$path/res/bbmap_all.sam ignorebadquality qin=33 \
mhist=$path/res/mhist.txt ehist=$path/res/ehist.txt \
indelhist=$path/res/indelhist.txt

## copy the fig/mhist.txt locally, plt does not well on bonette
codePath=/media/ssd/yan/code/accalign-benchmark/random-access
python $codePath/16groups/error-rate-per-pos.py $path/fig
```



## 2. Run


## 2.1 Inference with motif search (GDB example)
```shell
path=/media/ssd/ngs-data-analysis/motif-search-8payload-202305

gdb motif-search 
set history save on

set args -t 1 \
-r  /media/ssd/ngs-data-analysis/motif-search-8payload-202305/all_pass.fq  -i guppy \
-m /media/ssd/ngs-data-analysis/motif-search-8payload-202305/motif-lib.fa --n 10 \
--fp ACTTACCCGCTACTTTCATCACTAATTTACTCCCAAC \
--rp CCAACTACACTATACCACCTCGCATCATACCTACCTTA \
--s /media/ssd/ngs-data-analysis/motif-search-8payload-202305/spacers.fa --mode L \
-o /media/ssd/ngs-data-analysis/motif-search-8payload-202305/res.txt
```

## 2.2 Subsample the reads and check the accuracy 

#### Prepare the subsampled reads
`python accalign-benchmark/common/format/subsampleFq.py`

#### Decode
```
# inference
path=/media/ssd/ngs-data-analysis/motif-search-8payload-202305
cov=1

time /media/ssd/ngs-data-analysis/code/motif-search/motif-search -t 12 \
-r  $path/read/all_pass-cov${cov}.fq  -i guppy \
-m $path/motif-lib.fa --n 10 \
--fp ACTTACCCGCTACTTTCATCACTAATTTACTCCCAAC \
--rp CCAACTACACTATACCACCTCGCATCATACCTACCTTA \
--s $path/spacers.fa --mode L \
-o $path/res/res-cov${cov}.txt


## get top N frequent motif in each column
python /media/ssd/ngs-data-analysis/code/motif-search/script/202305-11spacer/get_top_freq.py \
res-cov${cov}.txt 

## check the accuracy -> output in $path/res/recover_accuracy-cov/...
python /media/ssd/ngs-data-analysis/code/motif-search/script/202305-11spacer/accuracy.py \
${cov}
```


