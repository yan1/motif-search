import sys
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

path = '/media/ssd/ngs-data-analysis/motif-search-8payload-202305/'
read_path = path + 'all_pass.fq'
# read_path = path + 'tst.fq'

read = []
i = 0
with open(read_path) as file_in:
    for line in file_in:
        if i % 4 == 1:
            read.append(line.strip())
        i = i + 1

read = pd.DataFrame(read)
read['len'] = read[0].str.len()

print('nb of reads:' + str(len(read)))
print('longest of read length:' + str(read['len'].max()))
print('25% read length:' + str(read['len'].quantile(0.25)))
print('median read length:' + str(read['len'].median()))
print('75% read length:' + str(read['len'].quantile(0.75)))


read_draw_ssp = read[read['len']<2000].copy()
print('% reads discarded (>2000nt): ' + str((len(read) - len(read_draw_ssp))/len(read)*100))

y, x, _ = plt.hist(read_draw_ssp['len'], bins=300)

print(x.max())
print(y.max())

ymin, ymax = y.min(), y.max()
plt.ylim(ymin, 1.2 * ymax)

plt.title('Histogram of read length')
plt.xlabel('Length of reads/nt')
plt.ylabel('Number of reads')
plt.savefig(path + '/his-len.pdf')