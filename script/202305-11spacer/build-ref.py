from __future__ import division

import pandas as pd
import sys
import csv
import itertools

fp = 'ACTTACCCGCTACTTTCATCACTAATTTACTCCCAACGCCTCTTATCCAA'
spacers = ['ATACTTTCATCCACGCCTCTTAATC', 'CCTATCTATCTCACACGCACTCTTC', 'CACATCCTACTTTCACCTACCCGCA',
           'CTTACTTTCCTACTCACTTCTCCAC', 'CTCACACACTTCTACTTACTACCAC', 'CTCCTATATACTACCCTTCATTCGC',
           'TTCTTCCATCCCACCATCGCACAAA', 'TTACCTTTTCTACCTTTATCCCCGC', 'TTACATACAACCCAGCTCCATCCCT']
rp = 'TCTCTGCCCATCCCAACTACACTATACCACCTCGCATCATACCTACCTTA'

path = '/media/ssd/ngs-data-analysis/motif-search-8payload-202305/'
motif_path = path + 'motif-lib.txt'
out_path = path + 'ref.fa'
nb_ref = 0

motif = []
with open(motif_path) as file_in:
    for line in file_in:
        motif.append(line.strip())

ref_path = path + "encoded.csv"
ref = pd.read_csv(ref_path, sep="\",\"",  header = 0, dtype = {'Payload1': list}, quoting=csv.QUOTE_NONE)
ref.rename(columns={'Payload8\"': 'Payload8'}, inplace=True)
ref["\"Address_Incrementer_1"] = ref["\"Address_Incrementer_1"].str[2:3]
ref["Address_Incrementer_2"] = ref["Address_Incrementer_2"].str[1:2]
ref["Payload8"] = ref["Payload8"].str[:-1]
ref = ref.values.tolist()

with open(out_path, 'w') as f:
    for cur in ref:
        cur[0] = [cur[0]]
        cur[1] = [cur[1]]
        for i in range(8):
            j = i + 2
            cur[j] = cur[j][1:-1].split(', ')
        #list all permutation
        permutation = list(itertools.product(*cur))
        # for per in permutation:
        for per in permutation:
            per = [motif[int(x)-1] for x in per]
            oligo = fp
            for i in range(9):
                oligo += per[i] + spacers[i]
            oligo += per[9] + rp
            f.write(">%s\n%s\n" % (nb_ref, oligo))
            nb_ref += 1


