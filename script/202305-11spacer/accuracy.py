from __future__ import division
import pandas as pd
import sys
import csv


cov = str(sys.argv[1])
file_name = "res-cov" + cov + "-"

path = "/media/ssd/ngs-data-analysis/motif-search-8payload-202305/res/"
ref_path = "/media/ssd/ngs-data-analysis/motif-search-8payload-202305/encoded.csv"
ref = pd.read_csv(ref_path, sep="\",\"",  header = 0, dtype = {'Payload1': list}, quoting=csv.QUOTE_NONE)
ref.rename(columns={'Payload8\"': 'Payload8'}, inplace=True)
ref["\"Address_Incrementer_1"] = ref["\"Address_Incrementer_1"].str[2:3]
ref["Address_Incrementer_2"] = ref["Address_Incrementer_2"].str[1:2]
ref["Payload8"] = ref["Payload8"].str[:-1]

res = []

for j in range(8):
    i = j + 2
    col_name = str(i)
    payload_name = "Payload" + str(i-1)
    print(col_name + '+' + payload_name)
    input_path =  path + file_name + "good_addr" + str(i) + ".csv"
    motifs = pd.read_csv(input_path, sep=",", header = 0, quoting=csv.QUOTE_NONE)
    motifs['0'] = motifs['0'].str[1:].astype(int) + 1
    motifs['0'] = motifs['0'].astype(str)
    motifs['1'] = motifs['1'].str[1:].astype(int) + 1
    motifs['1'] = motifs['1'].astype(str)
    motifs[col_name] = motifs[col_name].str[1:].astype(int) + 1
    motifs = motifs.groupby(['0', '1'])[col_name].apply(list).reset_index()
    motifs[col_name] = motifs[col_name].apply(sorted)
    motifs = motifs.sort_values(['1', '0'])
    merged = pd.merge(motifs, ref, how='outer', left_on=['0', '1'], right_on=["\"Address_Incrementer_1", "Address_Incrementer_2"])
    cmp_res = merged[col_name].astype(str) == merged[payload_name].astype(str)
    res.append([cmp_res.sum(), (~cmp_res).sum()])

res_df = pd.DataFrame(res)
res_df.rename(columns={0: 'nb of seq fully recovered', 1: 'nb of seq not fully recovered (if more than nb of address, meaning some address motifs are wrong)'}, inplace=True)
res_df.to_csv(path + 'recover_accuracy-cov'+ cov + '.csv', index = True)
